// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// %NISTDTST_string --
//   Returns the string containing the NISTDTST component.
//
function str = %NISTDTST_string ( this )
    str = []
    str($+1) = sprintf("NISTDTST Object:\n")
    str($+1) = sprintf("================")
    str($+1) = ""
    str($+1) = sprintf("name: %s\n", this.name);
    str($+1) = sprintf("category: %s\n", this.category);
    str($+1) = sprintf("description:\n");
    str = [str;formatWithTabs(this.description)]
    str($+1) = sprintf("reference:\n");
    str = [str;formatWithTabs(this.reference)]
    str($+1) = sprintf("datastring:\n");
    str = [str;formatWithTabs(this.datastring)]
    str($+1) = sprintf("model:\n");
    str = [str;formatWithTabs(this.model)]
    str($+1) = sprintf("modelNumberOfParameters: %s\n", _tostring(this.modelNumberOfParameters));
    str($+1) = sprintf("modelEq: %s\n", _tostring(this.modelEq));
    str($+1) = sprintf("modelPar: %s\n", _tostring(this.modelPar)); 
    str($+1) = sprintf("residualSumOfSquares: %s\n", _tostring(this.residualSumOfSquares));
    str($+1) = sprintf("residualStandardDev: %s\n", _tostring(this.residualStandardDev));
    str($+1) = sprintf("degreeFreedom: %s\n", _tostring(this.degreeFreedom));
    str($+1) = sprintf("numberOfObservations: %s\n", _tostring(this.numberOfObservations));
    str($+1) = sprintf("x: %s\n", _tostring(this.x));
    str($+1) = sprintf("y: %s\n", _tostring(this.y));
    str($+1) = sprintf("start1: %s\n", _tostring(this.start1));
    str($+1) = sprintf("start2: %s\n", _tostring(this.start2));
    str($+1) = sprintf("parameter: %s\n", _tostring(this.parameter));
    str($+1) = sprintf("standarddeviation: %s\n", _tostring(this.standarddeviation));
    str($+1) = sprintf("sampleMean: %s\n", _tostring(this.sampleMean));
    str($+1) = sprintf("sampleSTD: %s\n", _tostring(this.sampleSTD));
    str($+1) = sprintf("sampleAutocorr: %s\n", _tostring(this.sampleAutocorr));
endfunction

//
// _strvec --
//  Returns a string for the given vector.
//
function str = _strvec ( x )
    str = strcat(string(x)," ")
endfunction
function s = _tostring ( x )
    if ( x==[] ) then
        s = "[]"
    else
        n = size ( x , "*" )
        if ( n == 1 ) then
            s = string(x)
        else
            [nr,nc] = size(x)
            tx = typeof(x)
            s = msprintf("%d-by-%d %s matrix",nr,nc,tx)
        end
    end
endfunction

function outstr = formatWithTabs(str)
    nbrows = size(str,"r")
    outstr = []
    k = 0
    for i = 1 : nbrows
        k = k + 1
        outstr($+1) = sprintf("\t %s", str(i));
    end
endfunction



